package Bomba;

/**
 *
 * @autor RafaelAlvizo
 */
public class Gasolina {
    private int idGasolina;
    private String tipo;
    private String marca;
    private float precio;
    
    // Constructores
    public Gasolina(int idGasolina, String tipo, String marca, float precio) {
        this.idGasolina = idGasolina;
        this.tipo = tipo;
        this.marca = marca;
        this.precio = precio;
    }

    public Gasolina() {
        this.idGasolina = 0;
        this.tipo = "";
        this.marca = "";
        this.precio = 0.0f;
    }
    
    // Getters y Setters
    public int getIdGasolina() {
        return idGasolina;
    }

    public void setIdGasolina(int idGasolina) {
        this.idGasolina = idGasolina;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public float getPrecio() {
        return precio;
    }

    public void setPrecio(float precio) {
        this.precio = precio;
    }
    
    // Método para obtener información
    public String obtenerInformacion() {
        return "ID: " + this.idGasolina + ", Tipo: " + this.tipo + ", Marca: " + this.marca + ", Precio: $" + this.precio;
    }
}
