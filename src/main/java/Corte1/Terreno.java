/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Corte1;

/**
 *
 * @author RAFFA
 */
public class Terreno {
    private int numLote;
    private float ancho;
    private float largo;
    Object calcularPerimetro;

    // Constructores
    public Terreno() {
        this.numLote = 0;
        this.ancho = 0.0f;
        this.largo = 0.0f;
    }

    public Terreno(int numLote, float ancho, float largo) {
        this.numLote = numLote;
        this.ancho = ancho;
        this.largo = largo;
    }

    public Terreno(Terreno t) {
        this.numLote = t.numLote;
        this.ancho = t.ancho;
        this.largo = t.largo;
    }

    // Métodos getter y setter
    public int getNumLote() {
        return numLote;
    }

    public void setNumLote(int numLote) {
        this.numLote = numLote;
    }

    public float getAncho() {
        return ancho;
    }

    public void setAncho(float ancho) {
        this.ancho = ancho;
    }

    public float getLargo() {
        return largo;
    }

    public void setLargo(float largo) {
        this.largo = largo;
    }

    // Método para calcular el área
    public float calcularArea() {
        return this.ancho * this.largo;
    }

    // Método para verificar si el terreno es cuadrado
    public boolean esCuadrado() {
        return this.ancho == this.largo;
    }

    @Override
    public String toString() {
        return "Terreno{" +
                "numLote=" + numLote +
                ", ancho=" + ancho +
                ", largo=" + largo +
                '}';
    }

    Object calcularPerimetro() {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }
}

